# Fertilus App

An app for monthly cycle tracking and alert them if they are late, if their period is early or if it is unusually long (like if medical attention is required). The app also has birth-control pill reminders.